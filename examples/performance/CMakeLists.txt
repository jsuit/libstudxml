add_executable(performance driver.cxx time.cxx)
target_link_libraries(performance studxml)

add_executable(perf-expat expat.cxx time.cxx $<TARGET_OBJECTS:expat>)
target_include_directories(perf-expat PRIVATE ${PROJECT_SOURCE_DIR}/libstudxml/details/expat)
target_link_libraries(perf-expat studxml)

add_executable(perf-gen gen.cxx)
target_link_libraries(perf-gen studxml)